[![N|HealthDex](./src/assets/images/logo.png)](https://todeng.github.io/static/media/logo.23b9c66f.png)


The project represents Single Page App displaying ALL Pokemon with their avatar, stats, basic information, type, moves list and evolution information (Which Pokemon will they evolve into AND what Pokemon have they evolved from).
HealthDex consumes the following API: [PokéAPI](https://pokeapi.co/)

[Demo](https://todeng.bitbucket.io/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
The app is ready to be deployed!

### Project Structure

    .
    ├── public                  # Public folder
    ├── src                     # Source files 
    |    ├── api                # Folder with api modules
    |    |    └── PokeApi.js    # Module with sets of methods for interaction with [PokéAPI](https://pokeapi.co/)
    |    ├── assets             # Project assets (images and fonts)
    |    ├── components         # Project React components
    |    ├── hooks              # Custom React Hooks
    |    ├── stores             # Contains modules for storing data
    |    ├── utils              # Util modules
    |    ├── configs.json         # Application configuration params
    |    └── index.js           # JS root file
    ├── package-lock.json
    ├── package.json
    └── README.md  

### Brief code walk-through

1. All UI logic is described in a set of 'Components'
2. It was not decided to use 'redux' as the app is quite simple. A number of custom stores were implemented instead.
3. Logic for interaction with 3d-party API is placed in 'PokeAPI'
4. Configuration of the app can be adjusted in 'configs.js '
5. For production-ready application, I would also add unit and integrations tests.