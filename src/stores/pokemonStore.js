import fetchPokemon from './fetchPokemon'
import EntityStore from './EntityStore'

class PokemonStore extends EntityStore {
  _fetchEntity = (pokemonId) => {
    return fetchPokemon(pokemonId)
  }
}

export default new PokemonStore()
