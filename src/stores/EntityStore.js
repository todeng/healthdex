class EntityStore {
  entities = {}
  promises = {}

  _fetchEntity = (id) => {
    return Promise.resolve()
  }

  getById = async (id) => {
    if (this.promises[id]) {
      await this.promises[id]
    }

    if (this.entities[id]) {
      return this.entities[id]
    }

    this.promises[id] = this._fetchEntity(id)
    this.entities[id] = await this.promises[id]

    delete this.promises[id]

    return this.entities[id]
  }

  getAllEntities = async () => {
    await Promise.all(Object.values(this.promises))

    return Object.values(this.entities)
  }
}

export default EntityStore