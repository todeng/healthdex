import {
  getPokemon,
  getPokemonSpecies,
  getPokemonEvolutionChain,
  getPokemonDefaultFrontSpriteUrl,
} from './../api/PokeApi'
import { getEntityId } from './../utils/urlHelpers'
import * as measureFormats from './../utils/measureFormats'

const LANGUAGE = 'en'

const getLanguageValue = (languageItems, property) => {
  const item = languageItems.find(x => x.language.name === LANGUAGE)

  return item && item[property]
}

const getEntityIdFromUrl = url => {
  const matchResult = /\/(\d*)\/$/.exec(url)
  return matchResult ? Number.parseInt(matchResult[1]) : 0
}

const createPokedex = (pokemon, pokemonSpecies) => {
  return {
    id: pokemon.id,
    name: getLanguageValue(pokemonSpecies.names, 'name'),
    genus: getLanguageValue(pokemonSpecies.genera, 'genus'),
    types: pokemon.types.map(t => ({
      id: getEntityIdFromUrl(t.type.url),
      name: t.type.name,
    })),
    height: pokemon.height / 10,
    heightFormat: measureFormats.metersWithFeetsFormat(pokemon.height / 10),
    weight: pokemon.weight / 10,
    weightFormat: measureFormats.kilogramsWithPoundsFormat(pokemon.weight / 10),
    abilities: pokemon.abilities,
  }
}

const createStats = pokemon => {
  return pokemon.stats.map(s => ({
    name: s.stat.name,
    value: s.base_stat,
  }))
}

const addIdsToChainSpicies = chain => {
  chain.forEach(x => {
    x.species.id = getEntityId(x.species.url)

    addIdsToChainSpicies(x.evolves_to || [])
  })

  return chain
}

const createEvolutionChain = pokemonEvolutionChain => {
  pokemonEvolutionChain.chain = addIdsToChainSpicies([pokemonEvolutionChain.chain])[0]

  return pokemonEvolutionChain
}

const createMoves = pokemon => {
  return pokemon.moves.map(m => ({
    id: getEntityId(m.move.url),
    name: m.move.name,
  }))
}

export default async pokemonId => {
  const pokemon = await getPokemon(pokemonId)
  const pokemonImage = await getPokemonDefaultFrontSpriteUrl(pokemonId)
  const pokemonSpecies = await getPokemonSpecies(pokemonId)
  const pokemonEvolutionChain = await getPokemonEvolutionChain(
    getEntityId(pokemonSpecies.evolution_chain.url)
  )

  return {
    id: pokemonId,
    name: pokemon.name,
    image: pokemonImage,
    moves: createMoves(pokemon),
    stats: createStats(pokemon),
    pokedex: createPokedex(pokemon, pokemonSpecies),
    evolutionChain: createEvolutionChain(pokemonEvolutionChain),
  }
}
