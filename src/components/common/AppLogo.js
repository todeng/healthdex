import React from 'react'
import { Link } from 'react-router-dom'
import logo from './../../assets/images/logo.png'
import './AppLogo.scss'

const AppLogo = () => {
  return (
    <Link to={'/'} className="app-logo">
      <img src={logo} alt="HealthDex" />
    </Link>
  )
}

export default AppLogo
