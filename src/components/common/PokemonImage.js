import React, { useState } from 'react'
import classnames from 'classnames'
import { ClipLoader } from 'react-spinners'
import { getPokemonDefaultFrontSpriteUrl } from './../../api/PokeApi'

const PokemonImage = ({ pokemonId }) => {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)

  const imageUrl = getPokemonDefaultFrontSpriteUrl(pokemonId)

  const handleImageLoaded = () => {
    setLoading(false)
  }

  const handleImageErrored = () => {
    setError(true)
  }

  return (
    <div className="pokemon-image">
      <ClipLoader
        color={'#123abc'}
        loading={loading || error}
      />

      <img 
        src={imageUrl} alt=""
        className={classnames({ hidden: loading || error })}
        onLoad={handleImageLoaded}
        onError={handleImageErrored}
      />
    </div>
  )
}

export default PokemonImage
