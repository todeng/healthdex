import React from 'react'
import PokemonType from './PokemonType'
import './PokemonTypeList.scss'

const PokemonTypeList = ({ types, selectedTypeId }) => {
  return (
    <div className="pokemon-type-list">
      {types.map(t => (
        <PokemonType key={t.id} type={t} selected={t.id === selectedTypeId} />
      ))}
    </div>
  )
}

export default PokemonTypeList
