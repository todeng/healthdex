import React from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'
import './PokemonType.scss'

const PokemonType = ({ type, selected }) => {
  return (
    <Link
      to={{ pathname: '/', search: 'typeid=' + type.id }}
      className={classnames('pokemon-type', { selected: !!selected })}
    >
      {type.name.toUpperCase()}
    </Link>
  )
}

export default PokemonType
