import React from 'react'
import { Switch } from 'react-router'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import PokemonBoard from './PokemonBoard'

const AppRouter = ({ children }) => {
  return (
    <Router>
      {children}

      <Switch>
        <Route path="/" component={PokemonBoard} />
      </Switch>
    </Router>
  )
}

export default AppRouter
