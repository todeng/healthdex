import React from 'react'
import AppRouter from './AppRouter'
import AppLogo from './common/AppLogo'
import ErrorBoundary from './ErrorBoundary'
import './App.scss'

function App() {
  return (
    <div className="app-root">
      <ErrorBoundary>
        <AppRouter>
          <AppLogo />
        </AppRouter>
      </ErrorBoundary>
    </div>
  )
}

export default App
