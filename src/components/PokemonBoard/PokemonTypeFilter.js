import React from 'react'
import PokemonTypeList from '../common/PokemonTypeList'
import usePokemonTypes from '../../hooks/usePokemonTypes'

const PokemonTypeFilter = ({ typeId }) => {
  const { pokemonTypes } = usePokemonTypes()

  return (
    <div className="pokemon-type-filter">
      <h2>Pokemon Types</h2>

      <PokemonTypeList types={pokemonTypes} selectedTypeId={typeId} />
    </div>
  )
}

export default PokemonTypeFilter
