import React from 'react'
import numeral from 'numeral'
import classnames from 'classnames'
import PokemonTypeList from './../../common/PokemonTypeList'
import { capitalizeDashJoin } from './../../../utils/stringFormats'

const defaultPokedex = {
  id: 0,
  name: 'Loading...',
  genus: 'Loading...',
  types: [],
  heightFormat: 'Loading...',
  weightFormat: 'Loading...',
  abilities: [],
}

const PokemonPokedexData = ({ className, pokemonId, pokedex }) => {
  pokedex = { ...defaultPokedex, id: pokemonId, ...pokedex }

  const renderTableRow = (header, value) => {
    return (
      <tr>
        <th>{header}</th>
        <td>{value}</td>
      </tr>
    )
  }

  const renderAttributeList = abilities => {
    return (
      <ol>
        {abilities
          .sort((a, b) => a.slot - b.slot)
          .map(a => {
            return (
              <li key={a.slot}>
                {capitalizeDashJoin(a.ability.name)} {a.is_hidden && '(hidden)'}
              </li>
            )
          })}
      </ol>
    )
  }

  return (
    <div className={classnames('pokemon-board-item-pokedex', className)}>
      <h3>Pokédex data</h3>

      <table>
        <tbody>
          {renderTableRow('National №', numeral(pokedex.id).format('000'))}
          {renderTableRow('Name', pokedex.name)}
          {renderTableRow('Species', pokedex.genus)}
          {renderTableRow('Type', <PokemonTypeList types={pokedex.types} />)}
          {renderTableRow('Height', pokedex.heightFormat)}
          {renderTableRow('Weight', pokedex.weightFormat)}
          {renderTableRow('Abilities', renderAttributeList(pokedex.abilities))}
        </tbody>
      </table>
    </div>
  )
}

export default PokemonPokedexData
