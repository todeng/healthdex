import React from 'react'
import classnames from 'classnames'
import PokemonImage from '../../common/PokemonImage'

const PokemonBoardItemImage = ({ className, pokemonId, openModal }) => {
  const handleClick = event => {
    event.preventDefault()

    openModal && openModal()
  }

  return (
    <a
      className={classnames('pokemon-board-item-image', className)}
      onClick={handleClick}
      href="/pokemon"
    >
      <PokemonImage pokemonId={pokemonId} />
    </a>
  )
}

export default PokemonBoardItemImage
