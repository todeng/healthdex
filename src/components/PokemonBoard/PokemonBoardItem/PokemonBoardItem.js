import React from 'react'
import classnames from 'classnames'
import PokemonBoardItemImage from './PokemonBoardItemImage'
import PokemonBoardItemBaseStats from './PokemonBoardItemBaseStats'
import PokemonBoardItemPokedex from './PokemonBoardItemPokedex'
import usePokemon from '../../../hooks/usePokemon'

const PokemonBoardItem = ({ className, pokemonId, openModal }) => {
  const { pokemon } = usePokemon(pokemonId)

  const openPokemonModal = () => {
    openModal && openModal(pokemon.id)
  }

  return (
    <div className={classnames('pokemon-board-item', className)}>
      <div className="row">
        <PokemonBoardItemImage
          pokemonId={pokemonId}
          className="col-4"
          openModal={openPokemonModal}
        />
        <PokemonBoardItemPokedex pokedex={(pokemon || {}).pokedex} pokemonId={pokemonId} className="col-8" />
        <PokemonBoardItemBaseStats stats={(pokemon || {}).stats} className="col-12" />
      </div>
    </div>
  )
}

export default PokemonBoardItem
