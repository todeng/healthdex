import React from 'react'
import classnames from 'classnames'
import interpolate from 'color-interpolate'
import './ProgressBar.scss'

let colormap = interpolate(['#FF2626', '#FF7F0E', '#A0E515'])

const ProgressBar = ({ className, value, maxValue }) => {
  const ratio = value > maxValue ? maxValue : value / maxValue
  const color = colormap(ratio)
  const width = Math.round(ratio * 10000000) / 100000

  return (
    <div className={classnames('progress-bar', className)}>
      <div
        className="progress-thumb"
        style={{ width: `${width}%`, backgroundColor: color }}
      />
    </div>
  )
}

export default ProgressBar
