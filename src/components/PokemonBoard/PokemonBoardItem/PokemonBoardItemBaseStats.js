import React from 'react'
import classnames from 'classnames'
import ProgressBar from './ProgressBar'

const statLabels = {
  hp: 'HP',
  attack: 'Attack',
  defense: 'Defense',
  'special-attack': 'Sp. Atk',
  'special-defense': 'Sp. Def',
  speed: 'Speed',
}

const PokemonBoardItemBaseStats = ({ className, stats = [] }) => {
  const maxValue = Math.max(1, ...stats.map(s => s.value))
  const groupedStats = stats.reduce(
    (acc, stat) => (acc[stat.name] = stat) && acc,
    {}
  )

  const renderTableRow = (header, value) => {
    return (
      <tr key={header}>
        <th>{header}</th>
        <td className="value">{value}</td>
        <td className="bar">
          <ProgressBar value={value} maxValue={maxValue} />
        </td>
      </tr>
    )
  }

  return (
    <div className={classnames('pokemon-board-item-base-stats', className)}>
      <h3>Base Stats</h3>

      <table>
        <tbody>
          {Object.keys(statLabels).map(name =>
            renderTableRow(statLabels[name], (groupedStats[name] || {}).value || 0)
          )}
        </tbody>
      </table>
    </div>
  )
}

export default PokemonBoardItemBaseStats
