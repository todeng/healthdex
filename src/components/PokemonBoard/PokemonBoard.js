import React, { useState } from 'react'
import { withRouter } from 'react-router'
import { ClipLoader } from 'react-spinners'
import usePokemonListBaseOnLocation from '../../hooks/usePokemonListBaseOnLocation'
import PokemonBoardItem from './PokemonBoardItem'
import PokemonBoardNavigation from './PokemonBoardNavigation'
import PokemonTypeFilter from './PokemonTypeFilter'
import PokemonModal from '../modals/PokemonModal'
import './PokemonBoard.scss'

const PokemonBoard = ({ location }) => {
  const [modalPokemonId, setModalPokemonId] = useState(0)
  const { data, count, offset, limit, typeId, loading } = usePokemonListBaseOnLocation(
    location
  )

  const openPokemonModal = pokemonId => {
    setModalPokemonId(pokemonId)
  }

  const closePokemonModal = pokemonId => {
    setModalPokemonId(0)
  }

  return (
    <div className="pokemon-board">
      <PokemonTypeFilter typeId={typeId} />
      <PokemonBoardNavigation
        offset={offset}
        limit={limit}
        typeId={typeId}
        count={count}
      />

      <div className="pokemon-board-list row">
        {data.map(p => (
          <PokemonBoardItem
            openModal={openPokemonModal}
            className="col-sm-12 col-md-6 col-lg-4"
            key={p.id}
            pokemonId={p.id}
          />
        ))}

        { (!loading && data.length === 0) && <div className="no-data-found">No Data Found</div>}
      </div>

      <ClipLoader
        color={'#123abc'}
        loading={loading}
      />

      <PokemonBoardNavigation
        offset={offset}
        limit={limit}
        typeId={typeId}
        count={count}
      />

      <PokemonModal
        open={!!modalPokemonId}
        pokemonId={modalPokemonId}
        onClose={closePokemonModal}
      />
    </div>
  )
}

export default withRouter(PokemonBoard)
