import React from 'react'
import { Link } from 'react-router-dom'
import { createSearchString } from './../../utils/urlHelpers'

const PokemonBoardNavigation = ({
  limit = 0,
  offset = 0,
  count = 0,
  typeId = 0,
}) => {
  const hasPrevious = offset !== 0
  const hasNext = offset + limit <= count

  const nextSearch = createSearchString({
    offset: offset + limit,
    limit,
    typeId,
  })
  const prevSearch = createSearchString({
    offset: offset - limit,
    limit,
    typeId,
  })

  return (
    <div className="board-navigations">
      <Link
        to={{ pathname: '/', search: prevSearch }}
        disabled={!hasPrevious}
      >
        {'< Previous'}
      </Link>
      <Link
        to={{ pathname: '/', search: nextSearch }}
        disabled={!hasNext}
      >
        {'Next >'}
      </Link>
    </div>
  )
}

export default PokemonBoardNavigation
