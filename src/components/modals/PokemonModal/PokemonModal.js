import React from 'react'
import Modal from 'react-awesome-modal'
import { ClipLoader } from 'react-spinners'
import PokemonEvolutionChain from './PokemonEvolutionChain'
import PokemonMoveList from './PokemonMoveList'
import usePokemon from '../../../hooks/usePokemon'
import './PokemonModal.scss'

const PokemonModal = ({ pokemonId, open, onClose }) => {
  const { pokemon, loading } = usePokemon(pokemonId)
  const renderModalSpinner = () => (
    <ClipLoader
      sizeUnit={'px'}
      size={150}
      color={'#123abc'}
      loading={loading}
    />
  )

  const renderModalContent = () => {
    if (!pokemon) return null

    return (
      <div className="modal-content">
        <PokemonEvolutionChain evolutionChain={pokemon.evolutionChain} />
        <PokemonMoveList pokemon={pokemon} />
      </div>
    )
  }

  return (
    <Modal
      visible={open}
      effect="fadeInUp"
      onClickAway={onClose}
      className="pokemon-modal"
    >
      {loading && renderModalSpinner()}
      {!loading && renderModalContent()}
    </Modal>
  )
}

export default PokemonModal
