import React from 'react'
import { capitalizeDashJoin } from './../../../utils/stringFormats'
import './PokemonMoveList.scss'

const PokemonMoveList = ({ pokemon }) => {
  if (!pokemon) return null

  return (
    <div className="pokemon-move-list">
      <h2>Moves</h2>

      <div className="moves">
        {pokemon.moves.map(m => (
          <div className="move" key={m.id}>
            {capitalizeDashJoin(m.name)}
          </div>
        ))}
      </div>
    </div>
  )
}

export default PokemonMoveList
