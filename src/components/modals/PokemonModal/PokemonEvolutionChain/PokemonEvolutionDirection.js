import React from 'react'
import classnames from 'classnames'
import { capitalizeDashJoin } from './../../../../utils/stringFormats'

const triggers = {
  'level-up': ed => (ed.min_level ? `${ed.min_level} Level` : 'Level Up'),
  trade: () => 'Trade',
  'use-item': ed => `Use ${capitalizeDashJoin(ed.item.name)}`,
  shed: () => 'Shed',
}

const PokemonEvolutionDirection = ({ chain, number, count }) => {
  return (
    <div
      className={classnames(
        'pokemon-evolution-direction',
        `direction-${number}-${count}`
      )}
    >
      <div className="arrow" />
      <div className="text">
        {triggers[chain.evolution_details[0].trigger.name](
          chain.evolution_details[0]
        )}
      </div>
    </div>
  )
}

export default PokemonEvolutionDirection
