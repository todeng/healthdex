import React from 'react'
import PokemonEvolutionTreeJoint from './PokemonEvolutionChainJoint'
import './PokemonEvolutionChain.scss'

const PokemonEvolutionChain = ({ evolutionChain }) => {
  return (
    <div className="pokemon-evolution-chain">
      <h2>Evolution Chain</h2>

      <PokemonEvolutionTreeJoint chain={(evolutionChain || {}).chain} />
    </div>
  )
}

export default PokemonEvolutionChain
