import React from 'react'
import Pokemon from './Pokemon'
import PokemonEvolutionDirection from './PokemonEvolutionDirection'
import usePokemon from './../../../../hooks/usePokemon'

const PokemonEvolutionChainJoint = ({ chain, ...otherProps }) => {
  const species = (chain || {}).species || {}
  const { pokemon } = usePokemon(species.id)

  return (
    <div className="pokemon-evolution-chain-joint">
      <div className="from-species">
        <Pokemon pokemon={pokemon} pokemonId={species.id} />
      </div>
      <div className="directions">
        {chain.evolves_to.map((c, i) => (
          <PokemonEvolutionDirection
            key={i}
            number={i + 1}
            count={chain.evolves_to.length}
            chain={c}
          />
        ))}
      </div>
      <div className="to-species">
        {chain.evolves_to.map((c, i) => (
          <PokemonEvolutionChainJoint key={i} chain={c} />
        ))}
      </div>
    </div>
  )
}

export default PokemonEvolutionChainJoint
