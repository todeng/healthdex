import React from 'react'
import numeral from 'numeral'
import { capitalize } from './../../../../utils/stringFormats'
import PokemonImage from '../../../common/PokemonImage';

const Pokemon = ({ pokemon, pokemonId, loading }) => {
  const renderPokemonTypes = types => {
    return (
      <div className="type-list">
        {(types || []).map(t => (
          <div className="type" key={t.id}>
            {capitalize(t.name)}
          </div>
        ))}
      </div>
    )
  }

  return (
    <div className="pokemon">
      <PokemonImage pokemonId={pokemonId} />

      <div className="pokemon-number">
        {'#' + numeral(pokemonId).format('000')}
      </div>
      <div className="pokemon-name">{loading ? 'Loading...' : ((pokemon || {}).pokedex || {}).name || ''}</div>
      <div className="pokemon-types">
        {renderPokemonTypes(((pokemon || {}).pokedex || {}).types)}
      </div>
    </div>
  )
}

export default Pokemon
