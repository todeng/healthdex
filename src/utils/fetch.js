export default async (url, options) => {
  const response = await fetch(url, options)

  if (response.status === 200) {
    return await response.json()
  }

  throw new Error('HTTP error')
}
