export const capitalize = s => {
  return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()
}

export const capitalizeDashJoin = s => {
  return s
    .split('-')
    .map(capitalize)
    .join(' ')
}
