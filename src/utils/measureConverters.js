export const kilogramsToPounds = value => {
  return Math.round((value / 0.45359237) * 100) / 100
}

export const metersToFeets = value => {
  return value * 3.2808
}

export const feetsToInches = value => {
  return Math.round(value * 12)
}
