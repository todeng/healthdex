import queryString from 'query-string'

export const getEntityId = url => {
  const matchResult = /\/(\d*)\/+$/.exec(url)

  return matchResult ? Number.parseInt(matchResult[1]) : 0
}

export const getSearchParams = url => {
  return queryString.parse(url)
}

export const createSearchString = obj => {
  const params = Object.keys(obj)
    .filter(k => obj[k])
    .map(k => `${k.toLowerCase()}=${obj[k]}`)
    .join('&')

  return params ? '?' + params : ''
}
