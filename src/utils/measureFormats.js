import numeral from 'numeral'
import * as measureConverters from './measureConverters'

export const kilogramsWithPoundsFormat = kgs => {
  const lbs = measureConverters.kilogramsToPounds(kgs)

  return `${numeral(lbs).format('0.0')} lbs (${kgs} kg)`
}

export const metersWithFeetsFormat = meters => {
  const realFeet = measureConverters.metersToFeets(meters)
  const feet = Math.floor(realFeet)
  const inches = measureConverters.feetsToInches(realFeet - feet)

  return `${feet}'${numeral(inches).format('00')}" (${numeral(meters).format(
    '0.0'
  )} m)`
}
