import { useState, useEffect } from 'react'
import { getPokemons, getPokemonsByType } from '../api/PokeApi'

const fetchPokemonsByTypeId = async typeId => {
  const pokemons = await getPokemonsByType(typeId)

  return pokemons
}
const fetchPokemons = async () => {
  const resp = await getPokemons({ limit: 2000, offset: 0 })

  return resp.results
}

const loadedTypes = {}
let pokemons = []
let pokemonsGroupedById = {}
let promise = Promise.resolve()

export default (offset, limit, typeId) => {
  const [data, setData] = useState([])
  const [count, setCount] = useState(0)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const getPokemons = async () => {
      setLoading(true)

      if (pokemons.length === 0) {
        pokemons = await fetchPokemons()
        pokemons.forEach(p => (pokemonsGroupedById[p.id] = p))
      }

      if (typeId && !loadedTypes[typeId]) {
        const tpokemons = await fetchPokemonsByTypeId(typeId)

        tpokemons.forEach(
          p =>
            (pokemonsGroupedById[p.id].typeIds = [
              ...(pokemonsGroupedById[p.id].typeIds || []),
              typeId,
            ])
        )
        loadedTypes[typeId] = true
      }

      const newData = pokemons.filter(
        p => !typeId || (p.typeIds || []).includes(typeId)
      )

      setCount(newData.length)
      setData(newData.slice(offset, offset + limit))

      setLoading(false)
    }

    promise = promise.then(getPokemons)
  }, [offset, limit, typeId])

  return {
    data,
    count,
    loading,
  }
}
