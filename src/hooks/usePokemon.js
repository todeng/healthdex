import { useState, useEffect, useRef } from 'react'
import pokemonStore from '../stores/pokemonStore'

export default pokemonId => {
  const ref = useRef()
  const [pokemon, setPokemon] = useState()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    ref.current = false

    if (!pokemonId) return

    const getPokemon = async pokemonId => {
      setLoading(true)
      const result = await pokemonStore.getById(pokemonId)

      if (ref.current) return

      setPokemon(result)
      setLoading(false)
    }

    getPokemon(pokemonId)

    return () => {
      ref.current = true
    }
  }, [pokemonId])

  return { pokemon, loading }
}
