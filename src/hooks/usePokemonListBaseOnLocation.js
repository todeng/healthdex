import { useState, useEffect } from 'react'
import { getSearchParams } from './../utils/urlHelpers'
import usePokemonList from './usePokemonList'

const DEFAULT_OFFSET = 0
const DEFAULT_LIMIT = 12

const parseInt = (intStr, defaultValue = 0) => {
  const value = Number.parseInt(intStr)

  return isNaN(value) ? defaultValue : value
}

const getParamsFromUrl = location => {
  if (location && location.search) {
    const searchParams = getSearchParams(location.search)

    return {
      offset: parseInt(searchParams.offset, DEFAULT_OFFSET),
      limit: parseInt(searchParams.limit, DEFAULT_LIMIT),
      typeId: parseInt(searchParams.typeid, 0),
    }
  }

  return {
    offset: DEFAULT_OFFSET,
    limit: DEFAULT_LIMIT,
    typeId: 0,
  }
}

export default location => {
  const [offset, setOffset] = useState(0)
  const [limit, setLimit] = useState(DEFAULT_LIMIT)
  const [typeId, setTypeid] = useState(0)

  useEffect(() => {
    const params = getParamsFromUrl(location)

    setOffset(params.offset)
    setLimit(params.limit)
    setTypeid(params.typeId)
  }, [location])

  return {
    ...usePokemonList(offset, limit, typeId),
    offset,
    limit,
    typeId,
  }
}
