import { useState, useEffect } from 'react'
import { getPokemonTypes } from '../api/PokeApi'
import { getEntityId } from '../utils/urlHelpers'

let types = null
let promise = Promise.resolve()

const fetchPokemonTypes = async () => {
  const resp = await getPokemonTypes()

  return resp.results.map(t => ({
    id: getEntityId(t.url),
    name: t.name,
  }))
}

export default () => {
  const [pokemonTypes, setPokemonTypes] = useState([])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const initPokemonTypes = async () => {
      if (types) return types

      setLoading(true)
      setPokemonTypes(await fetchPokemonTypes())
      setLoading(false)
    }

    promise = promise.then(initPokemonTypes())
  }, [])

  return {
    pokemonTypes,
    loading,
  }
}
