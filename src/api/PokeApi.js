import fetch from '../utils/fetch'
import { getEntityId } from './../utils/urlHelpers'
import configs from '../configs.json'

export const getPokemonDefaultFrontSpriteUrl = pokemonId => {
  return configs.pokemonImageUrl.replace('%pokemonId%', pokemonId)
}

const apiFetch = async subUri => {
  return await fetch(configs.pokeApiUrl + subUri)
}

const addIdToItemsFromUrl = item => {
  return { ...item, id: getEntityId(item.url) }
}

export const getPokemons = async ({ limit = 20, offset = 0 }) => {
  const resp = await apiFetch(`/pokemon/?limit=${limit}&offset=${offset}`)

  resp.results = resp.results.map(addIdToItemsFromUrl)

  return resp
}

export const getPokemonsByType = async typeId => {
  const resp = await apiFetch(`/type/${typeId}`)

  const pokemons = resp.pokemon.map(p => addIdToItemsFromUrl(p.pokemon))

  return pokemons
}

export const getPokemonSpecies = async pokemonId => {
  const resp = await apiFetch(`/pokemon-species/${pokemonId}`)

  resp.id = pokemonId

  return resp
}

export const getPokemon = async pokemonId => {
  const resp = await apiFetch(`/pokemon/${pokemonId}`)

  resp.id = pokemonId

  return resp
}

export const getPokemonEvolutionChain = async evolutionChainId => {
  const resp = await apiFetch(`/evolution-chain/${evolutionChainId}`)

  resp.id = evolutionChainId

  return resp
}

export const getPokemonTypes = async () => {
  return await apiFetch('/type')
}
